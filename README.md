# **Setup of Gitlab to connect an External Docker registry**

#### Important Requirements: 

- YOU SHOULD HAVE **VALID SSL DOMAINS** for `registry` and `gitlab`

- Worked / Tested on Ubuntu and Debian.

- `Works with: WILDCARD SSL and DIFFERENT SSL for each domain`

### Setup Steps:

#### A. Configuration file of GITLAB SERVER:


- Please make sure you enabled the following lines in the `gitlab.rb` file:

    - Command:  `sudo nano /etc/gitlab/gitlab.rb`

```
#####################
# REGISTRY SETTINGS #
#####################
gitlab_rails['gitlab_default_projects_features_container_registry'] = true
gitlab_rails['registry_enabled'] = true
gitlab_rails['registry_host'] = "registry.example.com"
gitlab_rails['registry_issuer'] = "gitlab-issuer"
gitlab_rails['registry_api_url'] = "https://registry.example.com/"

# registry_external_url 'https://registry.example.com'
# gitlab_rails['registry_port'] = ""
# gitlab_rails['registry_path'] = "/var/opt/gitlab/gitlab-rails/shared/registry"

nginx['enable'] = true 
nginx['redirect_http_to_https'] = true 
nginx['hsts_max_age'] = 0
 
nginx['ssl_client_certificate'] = "/etc/gitlab/trusted-certs/ca.pem"

```

- This line will be explained later in the following Steps:
```
nginx['ssl_client_certificate'] = "/etc/gitlab/trusted-certs/ca.pem"
```

- Apply settings: `sudo gitlab-ctl reconfigure`

#### B. Next step on GITLAB SERVER:

- Go to `/var/opt/gitlab/gitlab-rails/etc` to find `gitlab-registry.key` file. 
    - With this file we will generate certs for docker registry.
```
> cd /var/opt/gitlab/gitlab-rails/etc
> openssl x509 -in ./req.pem -out ./registry-auth.crt -req -signkey ./gitlab-registry.key -days 3650
```


#### C. Next step on REGISTRY SERVER:

- Copy `registry-auth.crt` from Gitlab server to local folder of Registry data: `/registry_data/certs/registry-auth.crt`

- Run Registry with `docker-compose` using the following configuration (change `domain` `gitlab.example.com` with your own):

```
version: '2'

services:
  registry:
    container_name: example_registry
    image: registry:2
    restart: always
    ports:
    - "5000:5000"
    - "5443:443" 
    expose:
    - "5000" 
    - "5443"  
    environment: 
      REGISTRY_HTTP_ADDR: 0.0.0.0:443
      REGISTRY_AUTH_TOKEN_REALM: https://gitlab.example.com/jwt/auth 
      REGISTRY_AUTH_TOKEN_SERVICE: container_registry 
      REGISTRY_AUTH_TOKEN_ISSUER: gitlab-issuer 
      REGISTRY_AUTH_TOKEN_ROOTCERTBUNDLE: /certs/registry-auth.crt
      REGISTRY_HTTP_TLS_CERTIFICATE: /certs/domain.crt
      REGISTRY_HTTP_TLS_KEY: /certs/domain.key 
    volumes:
      - /registry_data/auth:/auth
      - /registry_data/data:/registry_data
      - /registry_data/certs:/certs

volumes:
  registry_data:
```


#### D. Add YOUR_ISSUER CA Public Certificate to your gitlab server recognized CA Authorities.

##### Add YOUR_ISSUER CA Public Certificate in trusted authorities.
###### !!! ( YOUR_ISSUER CA Public Certificate depends on SSL issuer, it may differ ) !!!

- Copy YOUR_ISSUER CA Public Certificate for example TERENA from: 
> [TERENA CA Public Certificate](https://pki.cesnet.cz/certs/TERENA_SSL_CA_3.pem)
    

- Edit File: `sudo nano /etc/gitlab/trusted-certs/ca.pem`

- Paste inside the certificate you copied.

- Edit File: `sudo nano /etc/ssl/certs/TERENA_CA.pem`

- Paste inside the certificate you copied.

- Update server ca-certificates: `sudo update-ca-certificates --fresh` 

##### !!! This will refresh the CA authority certificates that the system can recognize... !!!


# TROUBLESHOOTING:

- If delete `gitlab-registry.key` then you will regenerate it with `gitlab-ctl reconfigure` command... 
    - You will need to regenerate `registry-auth.crt` and reconnect `docker registry` to `gitlab server` by copying certificates and starting the docker-registry, as described in `Step: C`.
